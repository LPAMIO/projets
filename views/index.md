---
layout: layouts/page.njk
---

[sujets](sujets)

[rendus](rendus)

[site](jamstack)

15min +5 q + 5 mise en place

## Jury 1 : Abdellatif Bourjij et Anna Rici

heure|étudiant(s)|tuteur|entreprise
---|---|---|---
14h00|Kallan Munier et Romain Gouveia|Anna Ricci
14h25|Loïc Jaegle|Abdellatif Bourjij
14h50|Alan Paul||AREAL
15h15|Tieu Ly Hoang||NATURE.COS
15h40|*pause*|
16h00|Théodor Brown|Abdellatif Bourjij|G.C.S. DU KEMBERG
16h25|Ferreira Alexandre|Anna Ricci|INOVA WEB
17h50|Killian Vaubourg|Anna Ricci|ITELCOM
17h15|Kassandra Demaziere|Abdellatif Bourjij|GRANDES DISTILLERIES PEUREUX

## Jury 2 : Olivier Caspary et Emmanuel Medina
heure|étudiant(s)|tuteur|entreprise
---|---|---|---
14h00|Jade Chiari et Aurélien Schaegis|Olivier Caspary
14h25|Angel Thierry|Emmanuel Medina
14h50|Martin Tanguy et Alban Cavalli|Emmanuel Medina
15h15|Xavier Marchal et Florian Robert|Emmanuel Medina
15h40|*pause*
16h00|Gerson Torres|Emmanuel Medina
16h25|Nicolas Renard|Emmanuel Medina|Papeterie CLAIREFONTAINE
17h50|Petru Oltean|Emmanuel Medina|CENTRE HOSPITALIER GENERAL DE ST DIE
17h15|Kévin Blaise|Emmanuel Medina|Parabellum Software - OGICIEL
