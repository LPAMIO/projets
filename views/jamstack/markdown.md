---
layout: layouts/page.njk
title: Markdown
---


## Titres

```markdown
# Titre de niveau 1
## Titre de niveau 2
### Titre de niveau 3
#### Titre de niveau 4
##### Titre de niveau 5
###### Titre de niveau 6
```

## Séparations horizontales

Les lignes de séparation s'obtiennent avec une ligne composées de 3 tirets, 3 tirets bas ou 3 astérisques.

```markdown
___

---

***
```

___

---

***







## Emphase

La mise en *italique* s'obtient en entourant le texte avec une astérisque `*` ou un tiret bas `_`.

```
*Texte en italique*

_Texte en italique_
```

*Texte en italique*

_Texte en italique_

La mise en **gras** du texte s'obtient en entourant le texte avec 2 astérisques `*` ou 2 tiret bas `_`.

```markdown
**Texte en gras**

__Texte en gras__
```

**Texte en gras**

__Texte en gras__


Pour barrer du texte on l'entoure  avec 2 caractères tilde `~`.

```markdown
~~Texte barré~~
```

~~Texte barré~~

Le texte en gras et en italique, est entouré avec 3 astérisques ou 3 tirets bas.

```markdown
***Texte en gras et italique***

_**Texte en gras et italique **_
```

***Texte en gras et italique***

_**Texte en gras et italique**_


## Blockquotes

> Blockquotes can also be nested...
>> ...by using additional greater-than signs right next to each other...
> > > ...or with spaces between arrows.


## Listes

liste à points, non ordonnée

```markdown
+ Create a list by starting a line with `+`, `-`, or `*`
+ Sub-lists are made by indenting 2 spaces:
  - Marker character change forces new list start:
    * Ac tristique libero volutpat at
    + Facilisis in pretium nisl aliquet
    - Nulla volutpat aliquam velit
+ Very easy!
```

Liste triée

```
1. Lorem ipsum dolor sit amet
2. Consectetur adipiscing elit
3. Integer molestie lorem at massa

1. You can use sequential numbers...
1. ...or keep all the numbers as `1.`
```

Start numbering with offset:

```
57. foo
1. bar
```


## Code

Inline `code`

Indented code

    // Some comments
    line 1 of code
    line 2 of code
    line 3 of code


Block code "fences"

```
Sample text here...
```

Syntax highlighting

```js
var foo = function (bar) {
  return bar++;
};

console.log(foo(5));
```

## Tableaux

| Option | Description |
| ------ | ----------- |
| data   | path to data files to supply the data that will be passed into templates. |
| engine | engine to be used for processing templates. Handlebars is the default. |
| ext    | extension to be used for dest files. |

Right aligned columns

| Option | Description |
| ------:| -----------:|
| data   | path to data files to supply the data that will be passed into templates. |
| engine | engine to be used for processing templates. Handlebars is the default. |
| ext    | extension to be used for dest files. |


## Liens

```markdown
[link text](https://programmation-embarquee.netlify.app/)
```

[link text](https://programmation-embarquee.netlify.app/)

[link with title](https://programmation-embarquee.netlify.app/ "title text!")

Autoconverted link https://programmation-embarquee.netlify.app/ (enable linkify to see)


## Images

![Photo](photo.jpg)

![Photo](photo.jpg "Panda roux")

Like links, Images also have a footnote style syntax

![Alt text][id]

With a reference later in the document defining the URL location:

[id]: photo.jpg  "The Dojocat"

<img src="photo.jpg">

http://www.google.fr

## Typographic replacements

Enable typographer option to see result.

```markdown
(c) (C)
(r) (R)
(tm) (TM)
(p) (P)
+-
```

(c) (C) (r) (R) (tm) (TM) (p) (P) +-

## Plugins

The killer feature of `markdown-it` is very effective support of
[syntax plugins](https://www.npmjs.org/browse/keyword/markdown-it-plugin).

### Checklist

- [ ] unchecked
- [x] checked

```
- [ ] unchecked
- [x] checked
```

### [Emojies](https://github.com/markdown-it/markdown-it-emoji)

> Classic markup: :wink: :crush: :cry: :tear: :laughing: :yum:
>
> Shortcuts (emoticons): :-) :-( 8-) ;)

see [how to change output](https://github.com/markdown-it/markdown-it-emoji#change-output) with twemoji.


```
:wink: :crush: :cry: :tear: :laughing: :yum:
```

```
:-) :-( 8-) ;)
```


### [Subscript](https://github.com/markdown-it/markdown-it-sub) / [Superscript](https://github.com/markdown-it/markdown-it-sup)

- 19^th^
- H~2~O


### [\<ins>](https://github.com/markdown-it/markdown-it-ins)

++Inserted text++


### [\<mark>](https://github.com/markdown-it/markdown-it-mark)

==Marked text==


### [Footnotes](https://github.com/markdown-it/markdown-it-footnote)

Footnote 1 link[^first].

Footnote 2 link[^second].

Inline footnote^[Text of inline footnote] definition.

Duplicated footnote reference[^second].

[^first]: Footnote **can have markup**

    and multiple paragraphs.

[^second]: Footnote text.


### [Definition lists](https://github.com/markdown-it/markdown-it-deflist)

Term 1

: Definition 1
with lazy continuation.

Term 2 with *inline markup*

:   Definition 2

        { some code, part of Definition 2 }

    Third paragraph of definition 2.

_Compact style:_

Term 1
  ~ Definition 1

```
Term 1
  ~ Definition 1
```

Term 2
  ~ Definition 2a
  ~ Definition 2b


### [Abbreviations](https://github.com/markdown-it/markdown-it-abbr)

This is HTML abbreviation example.

It converts "HTML", but keep intact partial entries like "xxxHTMLyyy" and so on.

*[HTML]: Hyper Text Markup Language

### [Custom containers](https://github.com/markdown-it/markdown-it-container)

::: warning
*here be dragons*
:::
