---
layout: layouts/page.njk
title: Publication d'un site statique
---

Un site statique est un site dont la structure de la page n'est pas générée à chaque appel par un visiteur. Le contenu est lui le plus souvent fixe aussi mais pas nécessairement, il peut incorporer des appels à des services web mais ces appels seront effectués par le navigateur du client.

Pour générer le site et les pages nous allons utiliser [Eleventy](https://www.11ty.dev/) qui est un générateur de site statique simple à utiliser. Eleventy fonctionne à l'aide de NodeJs.

Il existe plusieurs solutions concurrentes, certaines plus rapides, plus évoluées, plus complètes, plus complexes, telles que : [Hugo](https://gohugo.io/), [Gatsby](https://www.gatsbyjs.com/), [Jekyll](https://jekyllrb.com/), [Nuxt](https://nuxtjs.org/fr/]), [Hexo](https://hexo.io/), ...

### Initialisation du projet

```shell-session
git init

npm init
```

### Installation d'Eleventy

```shell-session
npm install @11ty/eleventy
```

Configurer le fichier `.gitignore` pour ne pas inclure dans vos sources les fichiers de travail et de compilation. Seules vos sources à vous doivent figurer dans le dépôt.

```
public/
node_modules/
.env
.log
.cache
yarn.lock
.vscode
```

Configurer les options d'Eleventy

```javascript
module.exports = function(config) {

  return {
    dir: {
      input: "views",
      output: "public"
    }
  }
};
```


Syntaxe [markdown](markdown)


## Hébergement

### Gitlab Pages

Intégration continue

![](CI.png)

![](pages.png)

Fichier de configuration de Gitlab Pages

```yml
image: "node:lts-alpine"

pages:
  script:
  - npm install
  - npx @11ty/eleventy build --prefixpath=/projets/
  artifacts:
    paths:
    - public
  only:
  - main
```

### Netlify

L'hébergement des pages peut se faire avec Netlify.

Créer un compte chez Netlify.

Créer un nouveau projet Netlify associer le dépôt Git, Choisir Eleventy comme compilateur. Et voilà ! Netlify récupère les sources du projet, applique le script `eleventy` et publie les fichiers compilés du dossier `_site`.

![](netlify.png)

## Définitions

JAMStack
: JavaScript, APIs & Markup

SSR
: Server Side Rendering

CSR
: Client Side Rendering

SSG
: Static Site Generation

YAML
: Yet Another Markup Language ou de manière récursive YAML Ain't Markup Language. C'est un langage de balisage qui simplifie le format json en ne gardant que les clés et les valeurs. Les guillemets et accolades sont supprimés.

TOML
: Tom's Obvious, Minimal Language

## Informations externes
- [Sites statiques et Jamstack : la révolution frontend](https://buzut.net/sites-statiques-et-jamstack-la-revolution-frontend/)
- [C'est quoi la Jamstack au juste ?](https://jamstatic.fr/2019/02/07/c-est-quoi-la-jamstack/)
- [Qu’est-ce que la JAMstack et devrais-je m’en soucier ?](https://practicalprogramming.fr/what-is-jamstack/)

## Ressources externes
- [Eleventy](https://www.11ty.dev/)
- [Nunjucks](https://mozilla.github.io/nunjucks/)
- [Netlify](https://www.netlify.com/)
- [Mermaid](https://mermaid-js.github.io/mermaid/#/)

### Aller plus loin
- [Commment optimiser et charger les images en différé avec eleventy](https://mahmoudashraf.dev/blog/how-to-optimize-and-lazyloading-images-on-eleventy/)