---
layout: layouts/page.njk
title: Rendus
---

## Sources

Les sources du ou des projets devront être déposées sur une plateforme de gestion de code comme GitLab ou GitHub. Vous devrez inviter votre tuteur ainsi que l'utilisateur LPAMIO à voir vos sources.

## Rapport

Le rapport devra être publié sur un site web et être au format HTML. Voir le tutoriel pour la publication d'un [site statique](../jamstack)

## Cahier des charges fonctionnel

La première étape est de rédiger un cahier des charges fonctionnels qui devra comprendre :

La **demande du client**, l'expression de son besoin.
Pourquoi le client à ce besoin ? Qu'est ce qui n'est pas satisfaisant dans son environnement pour qu'il ai besoin de dépenser de l'argent dans un nouveau projet.

Une **étude concurrentielle**. Quels sont les concurrents à ce projet. Les points forts et les faiblesses de chaque produit. Vous pouvez le faire sous forme de tableau avec des plus et des moins pour chaque. En quoi ces produits ne remplissent pas toutes les conditions pour que votre client les choisisse ?

Un **descriptif des fonctionnalités** utilisateurs attendues. Que devra faire le programme vis à vis de l'utilisateur ?

Un **devis** c'est à dire le prix que devra payer le client pour votre réalisation. Mettez vous en situation réelle et considérez que vous devez gagner votre vie avec le projet.

Un **planning prévisionnel** de réalisation. Combien de temps estimez vous pour chaque partie du projet.

Ce cahier de charges est validé par le client. C'est le contrat de ce qui devra être réalisé.

## Présentation technique

Habituellement la présentation se fait lors de la **journée portes ouvertes**. Le projet doit être pleinement fonctionnel ce jour là. Vous présentez votre projet aux visiteurs ainsi qu'à votre tuteur.

## Soutenances
