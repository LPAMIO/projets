---
layout: layouts/page.njk
title: Sujets projets LP AMIO 2021-22
---

## Sujet 1 : Gestion de présence

### Partie 1

Concevoir une application pour gérer des événements. Les événements possèdent des séances planifiées à certaines dates. Les utilisateurs s'incrivent à un événement. Leur présence est contrôlée à chaque séance. L'application permet d'administrer des compétitions sportives, des entrainements, des séances de formation, des ateliers. Des résultats pourront être attachés aux événements.

### Partie 2

Réaliser un sytème de controle de la présence d'une personne en utilisant une authentification forte ou un dispositif biométrique.

***Tuteur*** : Abdellatif Bourjij

***Mot clés :*** événements, contrôle, e-ticket, biométrie

---
## Sujet 2 : Vélo connecté

### Partie 1

Concevoir une application de partage de vélo. Les utilisateurs inscrits peuvent réserver un type de vélo particulier. Concevoir une application mobile qui donne l'alerte en cas de chute.

### Partie 2

Réaliser un sytème de sécurité pour vélo, comprenant des indicateurs clignotants de direction, un feu d'alerte de freinange, une détection de chute, un anti-vol ...

***Tuteur*** : Emmanuel Medina

***Étudiants*** : Xavier Marchal et Florian Robert / Gerson Torres

**Rapport** : [https://velo-connecte-rapport.netlify.app/](https://velo-connecte-rapport.netlify.app/)

***Mot clés :*** sécurité, transport, mobilité durable, partage

---
## Sujet 3 : Plateforme d'apprentissage des langues

### Partie 1

Continuer l'application web existante. Ajout de fonctionnalitées dans la gestion des cours, intervenants, participants et ressources numériques.

### Partie 2

Réaliser une télécommande permettant de participer à un quizz minuté.

***Tuteur*** : Anna Ricci

***Étudiants*** : Kallan Munier et Romain Gouveia

**Rapport** : [https://kallan-munier.github.io/projet_amio_cdc/](https://kallan-munier.github.io/projet_amio_cdc/)

***Mot clés :*** formation, apprentissage

---
## Sujet 4 : Potager sous surveillance

### Partie 1

Concevoir une application qui permet de gérer une mini serre et le cycle de production de plantes alimentaires.

### Partie 2

Réaliser un système de surveillance des paramètres vitaux des plantes : luminosité, taux d'humidité, température. Concevoir un système d'arrosage automatique.

***Tuteur*** : Abdellatif Bourjij

***Étudiant*** : Loïc Jaegle

***Mot clés :*** culture, plantation, développement durable, santé

---
## Sujet 5 : Ambiance

### Partie 1

Concevoir une application qui répertorie les salles d'un bâtiment, l'occupation prévue de ces salles et qui restitue les paramètres d'ambiance des salles.

### Partie 2

Réaliser un sytème de mesure des paramètres d'ambiance d'un salle de cours : niveau sonore, taux de CO2, température, hygrométrie, luminosité, occupation ...

***Tuteur*** : Olivier Caspary

***Étudiants*** : Jade Chiari et  Aurélien Schaegis

**Rapport** : [https://schaegis5u.github.io/index.html](https://schaegis5u.github.io/index.html)

***Mot clés :*** pollution, stress, santé, bien-être

---
## Sujet 6 : Entraînement sportif

### Partie 1

Concevoir une application pour gérer des séances d'entraînement. Enregistrer des parcours, afficher les performances de chaque séance.

### Partie 2

Réaliser un système d'enregistrement d'un parcours sportifs : le parcours, la vitesse, la vitesse ascensionnelle, les signaux vitaux de l'athlète comme les pulsations cardiaques ...
Ce projet peut être un complément du sujet vélo connecté, si l'athlète est un cycliste.

***Tuteur*** : Emmanuel Medina

***Étudiant*** : Théodor Brown et Angel Thierry

***Mot clés :*** santé, performance, entraînement, parcours

**Rapport** : [https://theodorbrown.github.io/ProjetTutRepository/](https://theodorbrown.github.io/ProjetTutRepository/)

---
## Sujet 7 : Suivi de livraison

### Partie 1

Concevoir une application pour gérer des clients, des tournées de livraison le suivi de la réception des marchandises.

### Partie 2

Réaliser un système d'enregistrement d'une tournée de livraison. enregistrer le parcours, la température, les chocs pendant lee parcours. Concevoir une boite à colis partagée.

***Tuteur*** : Emmanuel Medina

***Étudiant*** : Martin Tanguy

**Rapport** : [https://tanguy41.gitlab.io/pt-lpamio/](https://tanguy41.gitlab.io/pt-lpamio/)

***Mot clés :*** transport, parcours, sécurité, traçage