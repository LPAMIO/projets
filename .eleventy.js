module.exports = function(config) {

  config.addPassthroughCopy("views/**/*.png");

  return {
    dir: {
      input: "views",
      output: "public"
    }
  }
};